import { createContext } from "react";

export const user = {
    firstname: 'Jane',
    lastname: 'Doe',
    email: 'jane.doe@gmail.com',
    password: '12345678',
    // logged: false,
    // setLogged
    address: {
        country: 'United States of America',
        city: 'New York',
    },
    avatar: 'https://www.freepik.com/blog/app/uploads/2019/08/Sin-t%C3%ADtulo-2.jpg',
    education: {
        higher: {
            university: 'Ufa State Aviation university',
            startStudy: '2008',
            endStudy: '2013',
            faculty: 'Life Safety in Technosphere',
            },
        school: {
            number: '9',
            city: 'Yelabuga',
            startStudy: '1998',
            endStudy: '2008',
        }
    },
    posts: [
        {
            id: 2,
            message: 'Today by 10 pm I will have graduated from Innopolis university!',

            time: 'Wednesday, December 15, 2021, 7:34 PM',
        },
        {
            id: 1,
            message: 'The new season of "Money heist" is insane!!!!',

            time: 'Sunday, December 12, 2021, 6:12 PM',
        },

        {
            id: 0,
            message: 'Hey! This is my first post in here',
            time: 'Thursday, December 2, 2021, 0:41 AM',
        },
    ]

};

export const UserContext = createContext(user)
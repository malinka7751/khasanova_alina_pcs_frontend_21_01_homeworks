import './Header.css'
import logo from "../../img/logo.svg"
import Nav from "./Nav/Nav";
import SearchIcon from '@mui/icons-material/Search';

const Header = () => {

    return(
        <div className='header'>
            <div className='header__container'>
                <div className='header__logo__bg'> <img src={logo} alt="logo"/></div>
                <Nav/>
                <div className='header__search'>
                    <SearchIcon className='search__icon'/>
                    <input className='search__input' type="text" placeholder={'Search on this page'}/>
                    <div className='search__button '>
                        <button className='button'>Log out</button>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Header
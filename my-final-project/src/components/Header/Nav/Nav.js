import './Nav.css';
import ContactPageIcon from '@mui/icons-material/ContactPage';
import SupervisedUserCircleIcon from '@mui/icons-material/SupervisedUserCircle';
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';
import FolderSharedIcon from '@mui/icons-material/FolderShared';
import {NavLink} from "react-router-dom";
const Nav = () => {
    return (
        <div className={'nav'}>
            <div className={'nav__item'}>
                <NavLink end to={'my_page'} className={({isActive}) => isActive ? 'nav__link_active nav__link' : 'nav__link'}>
                    <ContactPageIcon className={'nav__icon'}/>
                    My page
                </NavLink>
            </div>
            <div className={'nav__item'}>
                <NavLink to={'/friends'} className={({isActive}) => isActive ? 'nav__link_active nav__link' : 'nav__link'}>
                    <SupervisedUserCircleIcon className={'nav__icon'}/>
                    My friends </NavLink>
            </div>
            <div className={'nav__item'}>
                <NavLink to={'/photos'} className = {'nav__link'} href="#">
                    <PhotoCameraIcon className={'nav__icon'}/>
                    Photos
                </NavLink>
            </div>

            <div className={'nav__item'}>
                <NavLink to={'/documents'} className = {'nav__link'}
                   href="#">
                    <FolderSharedIcon className={'nav__icon'}/>
                    Documents
                </NavLink>
            </div>
        </div>
    )

}

export default Nav
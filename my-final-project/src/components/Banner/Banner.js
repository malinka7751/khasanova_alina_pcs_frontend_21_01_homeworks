
import {Avatar} from "@mui/material";
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import InfoLink from "./InfoLink/InfoLink";
import classes from './Banner.module.css'
import {useContext} from "react";
import {UserContext} from "../../contexts/UserContext";

const Banner = () => {
    const user = useContext(UserContext)
    return (
        <div>
            <div className={classes.container}>

                <div className={classes.info}>
                    <Avatar className={classes.avatar} src={user.avatar} alt='Jane Doe' style={{width: '200px', height: '200px'}}/>
                    <div className={classes.info__wrapper}>
                        <h2 className={classes.info__name}>{`${user.firstname} ${user.lastname}`}</h2>
                        <h3 className={classes.info__address}>{`${user.address.country}, ${user.address.city}`}</h3>
                        <div className={classes.info__links}>
                            <InfoLink Icon={YouTubeIcon}  color={'red'} bgColor={'white'} src={'http://youtube.com'}/>
                            <InfoLink Icon={TwitterIcon} color={'white'} bgColor={'#1976D2'} src={'http://twitter.com'}/>
                            <InfoLink Icon={FacebookIcon} color={'white'} bgColor={'#1976D2'} src={'http://facebook.com'}/>
                            <InfoLink Icon={InstagramIcon} color={'white'} bgColor={'#DE2D42'} src={'http://instagram.com'}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Banner




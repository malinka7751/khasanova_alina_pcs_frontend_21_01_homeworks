import './Forms.css'
import RegistrationForm from "./RegistrationForm/RegistrationForm";
import {useEffect, useState} from "react";
import SignInForm from "./SignInForm/SignInForm";

const Forms = (props) => {
    const [toggleState, setToggleState] = useState(1);
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetch('https://reqres.in/api/users')
            .then(response => response.json())
            .then(({total}) => {
                fetch(`https://reqres.in/api/users?per_page=${total}`)
                    .then(response => response.json())
                    .then(({data}) => {
                        setUsers(data)
                    })
                    .catch(err => alert(`Oops! something went wrong! ${err.message}`))
            })
            .catch(err => alert(`Oops! something went wrong! ${err.message}`))
    }, [])

    const toggleTab = (index) => {
        setToggleState(index);
        return index
    }
    return (
        <div className={'formsWrapper'}>
            <div className={'formTabs'}>
                <div className={toggleState === 1 ? 'formTabs__item formTabs__item_active' : 'formTabs__item'} onClick={() => toggleTab(1)}>Sign in</div>
                <div className={toggleState === 2 ? 'formTabs__item formTabs__item_active' : 'formTabs__item'} onClick={() => toggleTab(2)}>Create an account</div>
            </div>
            <RegistrationForm isActive={toggleState}/>
            <SignInForm isActive={toggleState} setLoggedIn={props.setLoggedIn} loggedIn={props.loggedIn} users={users}/>

        </div>
    )
}

export default Forms

import {Link} from "react-router-dom";
import {useState} from "react";

const Input = () => {
    const [email, setEmail] = useState('');
    const [emailWasChanged, setEmailWasChanged] = useState(false);
    const [emailError, setEmailError] = useState('The field should be filled');

    const emailHandler = (e) => {
        setEmail(e.target.value)
        if (e.target.value && !isValid(e.target.value)) {
            setEmailError('Email is not valid')
        } else if (!e.target.value) {
            setEmailError('The field should be filled')
        } else {
            setEmailError('')
        }
    }

    const blurHandler = (e) => {
        setEmailWasChanged(true)
        console.log(e.target, e.target.value, emailWasChanged)
    }


    const isValid = (email) => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    return (
        <form onSubmit={() => console.log ('submit')} className='input emailInput'>
            <label>
                <input
                    id={'email'}
                    type='text'
                    placeholder='E-mail'
                    value={email}
                    onBlur={e => blurHandler(e)}
                    onChange={e => emailHandler(e)}/>
            </label>
            {emailWasChanged && emailError && <div>{emailError}</div>}

            <input
                type="submit"
                value={'Send!'}
                onSubmit={e => console.log(e)}/>
            </form>

    )

}

export default Input

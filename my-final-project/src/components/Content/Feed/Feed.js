import classes from './Feed.module.css'
import ContentNavigation from "./ContentNavigation";

const Feed = () => {
    return (
        <div className={classes.feed}>
            <ContentNavigation/>
        </div>
    )
}

export default Feed
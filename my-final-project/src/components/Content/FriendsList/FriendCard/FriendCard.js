import {Avatar} from "@mui/material";
import './FriendCard.css'
const FriendCard = ({id, first_name, last_name, avatar, email, removeFriend}) => {

    return (
        <div id={id} className='friend__card' >
            <div className='friend__avatar'><Avatar style={{width: '100%', height: '100%'}} src={avatar} /></div>
            <div className='friend__info'>
                <h3 className='friend__name'>{`${first_name} ${last_name}`}</h3>
                <div className='friend__email'>{email}</div>
            </div>
            <div className='card__buttonGroupWrapper'>
                <div className='card__buttonWrapper'>
                    <button className='button'>Message</button>
                </div>
                <div className='card__buttonWrapper'>
                    <button className='button' onClick={e => removeFriend(id)}>Remove</button>
                </div>
            </div>
        </div>
    )
}

export default FriendCard
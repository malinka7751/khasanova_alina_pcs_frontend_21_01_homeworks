import MessageSender from "../MessageSender/MessageSender";
import Post from "../Post/Post";
import './MyPage.css'
import {useState, useContext} from "react";
import {UserContext} from "../../../contexts/UserContext";


const MyPage = () => {
    const user = useContext(UserContext)
    const initialPosts = user.posts
    const [posts, setPosts] = useState(initialPosts)
    const deletePost = (id) => {
        setPosts([...posts.filter((post) => (post.id !== id))])
    }
    return (
        <div className='myPage'>

            <MessageSender sendPost={setPosts} posts={posts}/>
            {posts.map(post =>  <Post key={post.id} deletePost={deletePost}
                post={post}/> )}
        </div>
    )
}

export default MyPage
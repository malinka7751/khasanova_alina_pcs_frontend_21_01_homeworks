import {Avatar} from "@mui/material";
import './Post.css'
import {useContext} from "react";
import {UserContext} from "../../../contexts/UserContext";
const Post = ({post, deletePost}) => {
    const user = useContext(UserContext)
    return (
        <div className='post'>
            <div className='post__header'>
                <div className='post__wrapper'>
                    <Avatar src={user.avatar} className='post__avatar'/>
                    <div className='post__info'>
                        <h3>{`${user.firstname} ${user.lastname}`}</h3>
                        <p>{post.time}</p>
                    </div>
                </div>
                <div className='post__delete' onClick={() => deletePost(post.id)}>&#10008;</div>
            </div>
            <p className='post__message'>{post.message} </p>

        </div>
    )
}

export default Post
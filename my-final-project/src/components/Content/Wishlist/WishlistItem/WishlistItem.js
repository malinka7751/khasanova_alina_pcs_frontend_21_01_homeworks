import './WishlistItem.css'

const WishlistItem = ({wish, removeWish}) => {

    return (
        <li className={'wishlistItem'} key={wish.id}>
            <span>
                {wish.text}
            </span>

            <div className='wishlistItem__delete' onClick={() => removeWish(wish.id)}>
                &#10008;
            </div>

        </li>
    )
}

export default WishlistItem
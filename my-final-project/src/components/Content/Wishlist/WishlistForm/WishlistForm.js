import {useState} from "react";

const WishlistForm = ({add}) => {
    const [wishInput, setWishInput] = useState('')

    const changeHandler = (e) => {
        setWishInput(e.currentTarget.value)
    }
    const clickHandler = (e) => {
        e.preventDefault()
        add(wishInput)
        setWishInput('')
    }
    const keydownHandler = (e) => {
        if (e.key === 'Enter') {
            clickHandler(e)
        }
    }
    
    return (
        <div>
            <input
                type="text"
                value={wishInput}
                onChange={changeHandler}
                onKeyPress={keydownHandler}
                placeholder={'Type the things you want to get as a present. Your friend are able to see the list'}/>
            <button onClick={clickHandler} style={{display: 'none'}}
            >g</button>
        </div>
    )

}

export default WishlistForm
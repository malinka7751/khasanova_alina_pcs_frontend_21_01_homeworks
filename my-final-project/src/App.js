import './App.css';
import Header from "./components/Header/Header";
import Banner from "./components/Banner/Banner";
import MyPage from "./components/Content/MyPage/MyPage";
import {Navigate, Outlet, Route, Routes} from "react-router-dom";
import Footer from "./components/Footer/Footer";
import Forms from "./components/Forms/Forms";
import Agreement from "./components/Content/Agreement";
import FriendsList from "./components/Content/FriendsList/FriendsList";
import { UserContext, user } from "./contexts/UserContext";
import Feed from "./components/Content/Feed/Feed";
import Wishlist from "./components/Content/Wishlist/Wishlist";
import {useState} from "react";
import Education from "./components/Content/Education/Education";
import Contacts from "./components/Content/Contacts/Contacts";
import About from "./components/Footer/About";
import Faq from "./components/Footer/Faq";
import Commercials from "./components/Footer/Commercials";
import Feedback from "./components/Footer/Feedback";


function App() {

    const [loggedIn, setLoggedIn] = useState( JSON.parse(localStorage.getItem('user'))?.email === user.email )

    return (
        <UserContext.Provider value={user}>
            <Routes>
                <Route path={'/'} element={<Layout/>}>
                    <Route index element={ loggedIn ? (<Navigate to='my_page'/>) : (<Forms setLoggedIn={setLoggedIn} loggedIn={loggedIn}/>)}/>
                    <Route path={'agreement'} element={<Agreement/>}/>
                    <Route path={'about'} element={<About/>}/>
                    <Route path={'faq'} element={<Faq/>}/>
                    <Route path={'friends'} element={<FriendsList/>}/>
                    <Route path={'commercials'} element={<Commercials/>}/>
                    <Route path={'feedback'} element={<Feedback/>}/>
                    <Route path={'my_page'} element={<MyPageLayout/>}>
                        <Route index element={<MyPage/>}/>
                        <Route path={'wishlist'} element={<Wishlist/>}/>
                        <Route path={'education'} element={<Education/>}/>
                        <Route path={'contacts'} element={<Contacts/>}/>
                    </Route>
                </Route>
            </Routes>
        </UserContext.Provider>
    );
}
function MyPageLayout() {
    return (
        <div className={'container'}>
            <Banner />
            <Feed/>
            <Outlet/>
        </div>
    )
}
function Layout() {
    return (
        <div className='app__body'>
            <Header/>
            <main className='main'>
                <Outlet/>
            </main>
            <Footer/>
        </div>
    )

}




export default App;
